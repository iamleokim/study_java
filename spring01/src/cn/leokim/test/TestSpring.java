package cn.leokim.test;
import cn.leokim.pojo.Category;
import cn.leokim.pojo.Product;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSpring {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext(
                new String[] { "applicationContext.xml" });

//        Category category = (Category) context.getBean("category");
//        System.out.println(category.getName());

        Product product = (Product) context.getBean("product");
        System.out.println(product.getName());
        System.out.println(product.getCategory().getName());

    }
}
