package cn.leokim.test;
import cn.leokim.pojo.Category;
import cn.leokim.pojo.Product;
import cn.leokim.service.ProductService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSpring {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext(new String[] { "applicationContext.xml" });

        ProductService ps = (ProductService) context.getBean("ps");
        ps.doSomeService();
    }
}
